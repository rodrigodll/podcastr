import { GetStaticProps, GetStaticPaths } from 'next'
import Link from 'next/link'
import Image from 'next/image'
import Head from 'next/head';

import { format, parseISO } from 'date-fns';
import ptBr from 'date-fns/locale/pt-BR'

import { api } from '../../services';
import { convertDurationToTimeString } from '../../utils';

import styles from './episode.module.scss';

import { usePlayer } from '../../contexts/PlayerContext'

type Episode = {
    id: string,
    title: string,
    members: string,
    thumbnail: string,
    description: string,
    publishedAt: string,
    duration: number,
    durationAsString: string,
    url: string
}

type EpisodeProps = {
    episode: Episode
}

export default function Episode ({ episode }: EpisodeProps) {
    console.log(episode)

    const { play } = usePlayer();

    return (
        <div className={styles.episode}>
            <Head>
                <title>{episode.title} | Podcastr</title>
            </Head>

            <div className={styles.thumbnailContainer}>
                <Link href="/">
                    <button type="button">
                        <img src="/arrow-left.svg" alt="voltar"/>
                    </button>
                </Link>

                <Image
                    width={700}
                    height={160}
                    src={episode.thumbnail}
                    alt={episode.title}
                    objectFit="cover"
                />

                <button type="button" onClick={() => play(episode)}>
                    <img src="/play.svg" alt="tocar"/>
                </button>
            </div>

            <header>
                <h1>{episode.title}</h1>
                <span>{episode.members}</span>
                <span>{episode.publishedAt}</span>
                <span>{episode.durationAsString}</span>
            </header>

            <div className={styles.description} dangerouslySetInnerHTML={{__html: episode.description }} />
        </div>
    )
}

export const getStaticPaths: GetStaticPaths = async () => {
    const { data } = await api.get('episodes', {
        params: {
            __limit: 2,
            __sort: 'published_at',
            __order: 'desc'
        }
    })

    const paths = data.map(episode => ({
        params: {
            slug: episode.id
        }
    }))

    return {
        paths,
        fallback: 'blocking'
    }
    // return {
    //     fallback: 'blocking' - gera de forma dinamica, todos os arquivos que nao foram gerados no build 
    //     fallback: false      - nao gera nenhum arquivos estático no build  
    //     fallback: true       - ? 

    // Quando se use fallback true, é necessário importar
    // import { useRouter } from 'next/router'

    // e fazer uma verificação antes do return que gera a página
    // const router = useRouter()
    // if(router.isFallback) {
    //     return <p>Carregando ...</p>
    // }
    // }
}

export const getStaticProps: GetStaticProps = async (ctx) => {
    const { slug } = ctx.params;

    console.log(ctx.params)

    const { data } = await api.get(`/episodes/${slug}`);

    const episode = {
        id: data.id,      
        title: data.title,
        members: data.members,
        thumbnail: data.thumbnail,
        description: data.description,
        publishedAt: format(parseISO(data.published_at), 'd MMM yy', { locale: ptBr}),
        duration: Number(data.file.duration),
        durationAsString: convertDurationToTimeString(Number(data.file.duration)),
        url: data.file.url
    }

    return {
        props: {
            episode,
        },
        revalidate: 60 * 60 * 24 // 24 horas
    }
}